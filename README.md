# Desktop JS App

Simple environment made in Electron to create apps for Windows, Linux and MacOS.

## __Requirements

* [NodeJS](https://nodejs.org/en/) - above v15.10.0
* [Yarn](https://yarnpkg.com/getting-started/install) - above 1.22.0

## __Installation

To install required modules simply type in your terminal:
```
yarn install
```

## __Usage

To run developer mode type:
```
yarn run serve
```

To build your app for windows type:
```
yarn run dist
```

If you want to build your app for specific platform pass the parameter:
* Windows: `--platform=win32`
* Linux: `--platform=linux`
* MacOS: `--platform=darwin`

```diff
- Remember that you have to run this scripts on platform for what you want to build -
```

For example if you want to make Linux app type:
```
yarn run dist --platform=linux
```